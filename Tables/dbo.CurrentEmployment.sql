CREATE TABLE [dbo].[CurrentEmployment]
(
[ind_source_id] [int] NULL,
[firmId] [int] NULL,
[firmName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[iaOnly] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[registrationBeginDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[firmBCScope] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[firmIAScope] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bdSECNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
