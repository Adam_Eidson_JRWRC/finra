CREATE TABLE [dbo].[Wilson-Davis List]
(
[RepCRD] [int] NULL,
[FirstName] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (61) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BDFirmCRD] [smallint] NULL,
[Branch_Address1] [varchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_Address2] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_City] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_ZipCode] [int] NULL,
[Branch_Phone] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessType] [varchar] (23) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DuallyRegisteredBDRIARep] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateBecameRep_Full] [datetime] NULL,
[DateOfHireAtCurrentFirm_Full] [datetime] NULL,
[Licenses] [varchar] (33) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_County] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
