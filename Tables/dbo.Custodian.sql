CREATE TABLE [dbo].[Custodian]
(
[CustodianId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SECNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedAt] [datetime] NOT NULL CONSTRAINT [DF_Custodian_CreatedAt] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Custodian] ADD CONSTRAINT [PK__Custodia__C347B4457D12A0CE] PRIMARY KEY CLUSTERED ([CustodianId]) ON [PRIMARY]
GO
