SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vIndividualLicenseFirm] as

select distinct 
i.CRDNumber as IndividualCRDNumber,
l.name as licenseName,
f.FINRANumber as FirmCRDNumber,
iff.StartDate as individualFirm_StartDate,
iff.EndDate as individualFirm_EndDate
from FINRA.dbo.Individual i
join FINRA.dbo.individual_License il on il.individualId = i.individualId
join FINRA.dbo.License l on l.licenseId = il.licenseId
join FINRA.dbo.individual_Firm iff on iff.individualID= i.individualID
join FINRA.dbo.Firm f on iff.firmId = f.firmId

GO
