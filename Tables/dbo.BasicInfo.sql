CREATE TABLE [dbo].[BasicInfo]
(
[firstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[lastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[middleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CRDNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dateBeginActive] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bcScope] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[iaScope] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
