SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vFirmAddress] as
Select distinct f.Name as FirmName,
f.FINRANumber as FirmFINRANumber,
Street1,
Street2,
City,
State,
PostalCode,
Country,
PhoneNumber,
Faxnumber,
MainAddress,
MailingAddress
From FINRA.dbo.Firm f
join FINRA.dbo.Firm_Address fa on f.FirmId = fa.FirmId
GO
