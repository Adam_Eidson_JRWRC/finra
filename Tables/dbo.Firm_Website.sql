CREATE TABLE [dbo].[Firm_Website]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[FirmId] [int] NOT NULL,
[Website] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedAt] [datetime] NOT NULL CONSTRAINT [DF_Firm_Website_CreatedAt] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Firm_Website] ADD CONSTRAINT [PK__Firm_Web__3214EC07C6556A42] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_114099447_2_3] ON [dbo].[Firm_Website] ([FirmId], [Website])
GO
CREATE FULLTEXT INDEX ON [dbo].[Firm_Website] KEY INDEX [PK__Firm_Web__3214EC07C6556A42] ON [FIrm_Website]
GO
ALTER FULLTEXT INDEX ON [dbo].[Firm_Website] ADD ([Website] LANGUAGE 1033)
GO
