SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vIndividualBranchAddress] as
SELECT  distinct ibl.Street1,
ibl.street2,
ibl.city,
ibl.state,
ibl.postalCode,
i.CRDNumber,
f.FINRANumber,
f.FINRAStatus,
f.SECStatus
  FROM [FINRA].[dbo].[Individual_BranchLocation] ibl
  join finra.dbo.Individual i on i.IndividualId = ibl.IndividualId
  join finra.dbo.firm f on f.FirmId = ibl.FirmId
  join finra.dbo.individual_firm iff on iff.FirmId =f.FirmId
  and iff.IndividualId = i.IndividualId
  where  iff.EndDate is null
GO
