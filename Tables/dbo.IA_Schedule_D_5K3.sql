CREATE TABLE [dbo].[IA_Schedule_D_5K3]
(
[Filing ID] [int] NULL,
[5K(3)(a)] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[5K(3)(b)] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[5K(3)(c) City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[5K(3)(c) State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[5K(3)(c) Country] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[5K(3)(d)] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[5K(3)(e)] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[5K(3)(f)] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[5K(3)(g)] [bigint] NULL
) ON [PRIMARY]
GO
