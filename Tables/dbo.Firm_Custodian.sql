CREATE TABLE [dbo].[Firm_Custodian]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[FirmId] [int] NOT NULL,
[CustodianId] [int] NOT NULL,
[RegulatoryAssetsUnderManagement] [decimal] (20, 2) NULL,
[CreatedAt] [datetime] NOT NULL CONSTRAINT [DF_Firm_Custodian_CreatedAt] DEFAULT (getdate()),
[FilingDateSubmitted] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Firm_Custodian] ADD CONSTRAINT [PK__Firm_Cus__3214EC072D45EB0D] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
