SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 11/08/2019
-- Description:	<Description,,>
-- =============================================
--exec FINRA.dbo.NewContactsAtFirmsWithSDA
CREATE PROCEDURE [dbo].[NewContactsAtFirmsWithSDA]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT  distinct [crd__c]
      ,[firm_crd__c]
	  ,firm_SFId
	  into #temp_SF
  FROM [FINRA].[dbo].[vSalesForceContactFirmWithSellingAgreement]
  where firm_crd__c is not null

  select distinct vifr.FirstName,
  vifr.LastName,
  vifr.CRDNumber,
  vifr.FINRALink,
  vifr.SECLink,
  vifr.FirmFINRANumber,
  vifr.FINRAStatus,
  sfFirm.Firm_SFId
  from vIndividualFirmRelationship vifr
  join #temp_SF sfFirm on sfFirm.firm_Crd__c = vifr.FirmFINRANumber
  left join #temp_SF cscf on cscf.crd__c = vifr.CRDNumber
								and cscf.firm_crd__c = vifr.FirmFINRANumber
where cscf.crd__c is null
and (vifr.SECStatus = 'active' or vifr.FINRAStatus = 'active')
and (vifr.FirmFINRAStatus = 'active' or vifr.FirmSECStatus = 'active')
and vifr.individualFirm_EndDate is null
and vifr.individualFirm_StartDate is not null
END
GO
