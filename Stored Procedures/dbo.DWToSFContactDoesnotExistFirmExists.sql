SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 10/18/2019
-- Description:	<Description,,>
-- =============================================
--exec FINRA.dbo.DWToSFContactDoesnotExistFirmExists
CREATE PROCEDURE [dbo].[DWToSFContactDoesnotExistFirmExists]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--individual does not exist
--Firm exists

select distinct i.CRDNumber as Contact_CRD,
i.FirstName,
i.LastName,
i.SECLink,
i.FINRALink,
f.FINRANumber as Firm_CRD,
f.Name as FirmName,
iff.startDate as firmStartdate,
iff.endDate as firmEndDate,
f.HighNetWorthAuM,
f.NumberOfHighNetWorthClients,
f.FINRAStatus as Firm_FinraStatus,
vsf.*,
i.SECStatus as individualSECStatus,
i.FINRAStatus as individualFINRAStatus,
i.dateBeganInIndustry
into #temp_NewContact
from FINRA.dbo.Individual i 
join FINRA.dbo.individual_Firm iff on iff.individualID= i.individualID
join FINRA.dbo.individual_License il on il.individualID = i.individualId
join FINRA.dbo.License l on l.licenseId = il.licenseId
join FINRA.dbo.Firm f on iff.firmId = f.firmId
								and iff.endDate is null
left join vSalesForceContactFirm vsf on i.CRDNumber= vsf.CRD__c
where l.name in ('Series 22', 'Series 65', 'Series 7')
and vsf.CRD__c is null
and startdate is not null

;with cte_SF_Broker_Dealer__c as 
(
select *
From openquery(SALESFORCEDEVART,'Select id, Firm_CRD__c from Broker_Dealer__c where Firm_CRD__c is not null and ER_Signed_Selling_Agreements__c is not null')
)

Select distinct *
From #temp_Newcontact nc
left join cte_SF_Broker_Dealer__c c on c.FIRM_CRD__c = nc.FIRM_CRD
where c.Firm_CRD__c is not null
END

GO
