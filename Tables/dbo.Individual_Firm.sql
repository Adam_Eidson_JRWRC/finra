CREATE TABLE [dbo].[Individual_Firm]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[FirmId] [int] NOT NULL,
[IndividualId] [int] NOT NULL,
[StartDate] [date] NULL,
[EndDate] [date] NULL,
[CreatedAt] [datetime] NOT NULL CONSTRAINT [DF_Individual_Firm_CreatedAt] DEFAULT (getdate()),
[UpdatedAt] [datetime] NOT NULL CONSTRAINT [DF_Individual_Firm_UpdatedAt] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Individual_Firm] ADD CONSTRAINT [PK__Individu__3214EC07EEE14DE4] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_Individual_Firm_5_1733581214__K5_K3_K2_K4] ON [dbo].[Individual_Firm] ([EndDate], [IndividualId], [FirmId], [StartDate]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1733581214_5_2] ON [dbo].[Individual_Firm] ([EndDate], [FirmId])
GO
CREATE STATISTICS [_dta_stat_1733581214_2_3] ON [dbo].[Individual_Firm] ([FirmId], [IndividualId])
GO
ALTER TABLE [dbo].[Individual_Firm] ADD CONSTRAINT [FK_Individual_Firm_Firm] FOREIGN KEY ([FirmId]) REFERENCES [dbo].[Firm] ([FirmId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Individual_Firm] ADD CONSTRAINT [FK_Individual_Firm_Individual] FOREIGN KEY ([IndividualId]) REFERENCES [dbo].[Individual] ([IndividualId]) ON DELETE CASCADE
GO
