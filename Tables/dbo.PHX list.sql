CREATE TABLE [dbo].[PHX list]
(
[RepCRD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BDFirmCRD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_ZipCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_County] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DuallyRegisteredBDRIARep] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateBecameRep_Full] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateOfHireAtCurrentFirm_Full] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
