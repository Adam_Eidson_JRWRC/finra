SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 10/31/2019
-- Description:	<Description,,>
-- =============================================
--exec FINRA.dbo.DWToSFContactNoLongerAtFirm
CREATE PROCEDURE [dbo].[DWToSFContactNoLongerAtFirm]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--individual exists
--Individual no longer works at firm listed in SF
with cte_initialResults as (
select distinct vsf.*,
i.IndividualCRDNumber,
i.FirmCRDNumber,
i.individualFirm_StartDate,
i.individualFirm_EndDate
from vSalesForceContactFirm vsf
left join vIndividualLicenseFirm i on i.IndividualCRDNumber= vsf.CRD__c
and vsf.firm_crd__c = i.FirmCRDNumber
where i.Licensename in ('Series 22', 'Series 65', 'Series 7')
--and i.individualCRDNumber is null
and i.individualFirm_EndDate is not null
), cte_currentFirm as (
select *
from vIndividualLicenseFirm i
where i.individualFirm_EndDate is null
)

select *
From cte_initialResults cir
left join cte_currentFirm ccf on cir.crd__c = ccf.individualCRDNumber
								and cir.firm_crd__c = ccf.FirmCRDNumber
where ccf.individualCRDNumber is null
END
GO
