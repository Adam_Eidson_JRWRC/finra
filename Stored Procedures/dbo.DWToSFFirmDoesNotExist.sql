SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 10/18/2019
-- Description:	<Description,,>
-- =============================================
--exec FINRA.dbo.DWToSFFirmDoesNotExist
CREATE PROCEDURE [dbo].[DWToSFFirmDoesNotExist]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--individual does not exist
--Firm does not exist

select distinct 
f.FINRANumber,
f.Name as FirmName,
f.HighNetWorthAuM,
f.NumberOfHighNetWorthClients,
f.FINRAStatus as Firm_FinraStatus,
--fw.website,
vsf.*
into #temp_NewFirm
from FINRA.dbo.Firm f 
--left join FINRA.dbo.firm_website fw on fw.firmId = f.firmId
left join vSalesForceContactFirm vsf on f.FINRANumber= vsf.Firm_CRD__c
where vsf.Firm_CRD__c is null
and highNetWorthAUM is not null

;with cte_SF_Broker_Dealer__c as 
(
SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT Firm_CRD__c FROM Broker_Dealer__c where Firm_CRD__c is not null')
)

Select distinct *
From #temp_NewFirm nc
left join cte_SF_Broker_Dealer__c c on c.FIRM_CRD__c = nc.FINRANumber
where c.Firm_CRD__c is null
END


GO
