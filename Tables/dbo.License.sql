CREATE TABLE [dbo].[License]
(
[LicenseId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[License] ADD CONSTRAINT [PK__License__72D60082E37D3670] PRIMARY KEY CLUSTERED ([LicenseId]) ON [PRIMARY]
GO
