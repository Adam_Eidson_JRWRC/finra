SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vNewConcordeReps] as
with cte_currentConcordeReps as (
select distinct i.IndividualId,
i.CRDNumber as IndividualCRDNumber,
i.FINRALink,
i.FINRAStatus as IndividualFINRAStatus,
i.FirstName,
i.LastName,
i.SECLink as IndividualSECLink,
iff.StartDate as CurrentFirmStartDate,
iff.EndDate as CurrentFirmEndDate,
f.Name as CurrentFirmName,
f.FINRANumber as CurrentFirmFINRANumber
From Individual_Firm iff 
join firm f on f.firmId = iff.firmId
join individual i on i.IndividualId = iff.IndividualId
where f.FINRANumber = 151604--f.name like '%Concorde%'
and EndDate is null
), finalRankReps as (
select currentReps.*,
iff.StartDate,
iff.EndDate,
DENSE_RANK() OVER(Partition By iff.individualId order by iff.EndDate desc) as row_num,
f.*
From cte_currentConcordeReps currentReps
join Individual_Firm iff on iff.IndividualId = currentReps.IndividualId
join Firm f on f.firmId = iff.firmId
where iff.EndDate is not null
)
select distinct *
From finalRankReps
where row_num = 1
and Name <> 'CONCORDE INVESTMENT SERVICES, LLC'
GO
