SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vIndividualLicense] as

select distinct 
i.CRDNumber as IndividualCRDNumber,
l.name as licenseName
from FINRA.dbo.Individual i
join FINRA.dbo.individual_License il on il.individualId = i.individualId
join FINRA.dbo.License l on l.licenseId = il.licenseId

GO
