CREATE TABLE [STAGE].[SFEmailDomainFirmWebsiteRelationship]
(
[SF_domain] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Contact_Id] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Contact_Name] [varchar] (121) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[website] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FINRANumber] [int] NULL,
[firmName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
