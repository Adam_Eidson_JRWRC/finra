CREATE TABLE [STAGE].[Individual]
(
[CRDNumber] [int] NULL,
[name] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bcScope] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[iaScope] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dateBeganInIndustry] [date] NULL
) ON [PRIMARY]
GO
