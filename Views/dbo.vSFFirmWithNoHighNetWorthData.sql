SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view [dbo].[vSFFirmWithNoHighNetWorthData] as
select distinct sf.id as SF_Broker_Dealer_ID,
f.FINRANumber,
F.HighNetWorthAuM,
F.NumberOfHighNetWorthClients
From Firm f
join (
SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT Firm_CRD__c, id , Firm_High_Net_Worth_AUM__c
FROM Broker_dealer__c where Firm_CRD__c is not null and Firm_High_Net_Worth_AUM__c is null') ) sf on sf.Firm_CRD__c = f.FINRANumber
where f.HighNetWorthAuM is not null
GO
