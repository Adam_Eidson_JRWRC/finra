CREATE TABLE [STAGE].[Individual_Firm]
(
[FirmCRD] [int] NOT NULL,
[IndividualCRD] [int] NOT NULL,
[StartDate] [date] NULL,
[EndDate] [date] NULL
) ON [PRIMARY]
GO
