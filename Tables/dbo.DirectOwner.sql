CREATE TABLE [dbo].[DirectOwner]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[FirmId] [int] NOT NULL,
[IndividualId] [int] NULL,
[Name] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Role] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DirectOwner] ADD CONSTRAINT [PK__DirectOw__3214EC0725B66F9F] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DirectOwner] ADD CONSTRAINT [FK_DirectOwner_Firm] FOREIGN KEY ([FirmId]) REFERENCES [dbo].[Firm] ([FirmId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DirectOwner] ADD CONSTRAINT [FK_DirectOwner_Individual] FOREIGN KEY ([IndividualId]) REFERENCES [dbo].[Individual] ([IndividualId]) ON DELETE CASCADE
GO
