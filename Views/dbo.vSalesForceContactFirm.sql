SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vSalesForceContactFirm]
as
with cte_Contact as (
SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT CRD__c, Broker_Dealer__c FROM Contact where CRD__c is not null')
), cte_Broker_Dealer as (
SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT id, Firm_CRD__c FROM Broker_Dealer__c where Firm_CRD__c is not null')
)

select c.crd__c,
bd.firm_crd__c
From cte_Contact c
left join cte_Broker_Dealer bd on bd.Id = c.Broker_Dealer__c
GO
