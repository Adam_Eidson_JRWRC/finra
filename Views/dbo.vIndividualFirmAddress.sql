SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vIndividualFirmAddress] as

select distinct fa.Street1,
fa.Street2,
fa.City,
fa.State,
fa.postalCode,
fa.country,
f.FINRANumber as FirmCRDNumber,
i.CRDNumber as IndividualCRDNumber,
i.firstName+' '+i.lastName as IndividualFullName
from FINRA.dbo.Firm f 
join FINRA.dbo.Individual_Firm ifirm on ifirm.FirmId = f.firmId
join FINRA.dbo.Individual i on i.individualId  = ifirm.individualId
join FINRA.dbo.Firm_Address fa on fa.firmId = f.firmId
where HighNetWorthAUM is not null
and ifirm.endDate is null
and City <> ''
GO
