SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vSalesForceContactFirmWithSellingAgreement]
as
with cte_Contact as (
SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT CRD__c, Broker_Dealer__c FROM Contact where CRD__c is not null')
), cte_Broker_Dealer as (
SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT id, Firm_CRD__c, ER_Signed_Selling_Agreements__c FROM Broker_Dealer__c where Firm_CRD__c is not null and (ER_Signed_Selling_Agreements__c is not null or ER_Signed_Selling_Agreements__c != ''none'')')
)

select c.crd__c,
bd.firm_crd__c,
bd.ER_Signed_Selling_Agreements__c,
bd.id as Firm_SFId
From cte_Contact c
join cte_Broker_Dealer bd on bd.Id = c.Broker_Dealer__c
where firm_crd__c is not null
  and cast(ER_Signed_Selling_Agreements__c as varchar(500))!= 'None'
GO
