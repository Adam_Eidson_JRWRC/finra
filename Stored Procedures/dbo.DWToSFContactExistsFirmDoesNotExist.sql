SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 10/18/2019
-- Description:	<Description,,>
-- =============================================
--exec FINRA.dbo.DWToSFContactExistsFirmDoesNotExist
CREATE PROCEDURE [dbo].[DWToSFContactExistsFirmDoesNotExist]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--individual exists
--Firm does not exist
select distinct i.CRDNumber as Contact_CRD,
i.FirstName,
i.LastName,
i.SECLink,
i.FINRALink,
f.FINRANumber as Firm_CRD,
f.Name as FirmName,
iff.startDate as firmStartdate,
iff.endDate as firmEndDate,
f.HighNetWorthAuM,
f.NumberOfHighNetWorthClients,
vsf.*
from FINRA.dbo.Individual i 
join FINRA.dbo.individual_Firm iff on iff.individualID= i.individualID
join FINRA.dbo.individual_License il on il.individualID = i.individualId
join FINRA.dbo.License l on l.licenseId = il.licenseId
join FINRA.dbo.Firm f on iff.firmId = f.firmId
								and iff.endDate is null
left join vSalesForceContactFirm vsf on i.CRDNumber= vsf.CRD__c
where l.name in ('Series 22', 'Series 65', 'Series 7')
and vsf.CRD__c is not null
and isnull(vsf.Firm_CRD__c,'') <> f.FINRANumber
END
GO
