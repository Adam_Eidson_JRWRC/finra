SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create View [dbo].[vSalesForceRIAAndRepImport] as (
select distinct i.CRDNumber,
i.FirstName,
i.LastName,
i.FINRAStatus,
i.SECStatus,
i.dateBeganInIndustry,
l.Name as licenseName,
case when i.FINRAStatus = 'Active' and i.SECStatus = 'Active'
	then 'Dually Registered'
	when i.FINRAStatus = 'Active' and i.SECStatus <> 'Active'
	then 'Broker-Dealer'
	when i.FINRAStatus <> 'Active' and i.SECStatus = 'Active'
	then 'RIA'
	when i.finraStatus is null and i.secStatus is null
	then 'RIA'
end as ContactRecordType,
f.Name as FirmName,
f.FINRANumber as FirmCRDNumber,
ifirm.startDate as firmStartDate
from FINRA.dbo.Individual i
join FINRA.dbo.individual_License il on il.individualId = i.individualId
join FINRA.dbo.License l on l.licenseId = il.licenseId
join FINRA.dbo.Individual_Firm ifirm on ifirm.individualId = i.individualId
join FINRA.dbo.firm f on f.firmId = ifirm.firmId
where ifirm.endDate is null
and ifirm.startDate is not null
)
GO
