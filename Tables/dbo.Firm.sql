CREATE TABLE [dbo].[Firm]
(
[FirmId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FINRANumber] [int] NULL,
[CreatedAt] [datetime] NULL,
[updatedAt] [datetime] NULL,
[FINRAStatus] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECStatus] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighNetWorthAuM] [decimal] (20, 2) NULL,
[NumberOfHighNetWorthClients] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Firm] ADD CONSTRAINT [PK__Firm__1F1F209C6EE652EA] PRIMARY KEY CLUSTERED ([FirmId]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1605580758_4_2_9_10_7] ON [dbo].[Firm] ([FINRANumber], [Name], [HighNetWorthAuM], [NumberOfHighNetWorthClients], [FINRAStatus])
GO
CREATE STATISTICS [_dta_stat_1605580758_1_4_2_9_10_7] ON [dbo].[Firm] ([FirmId], [FINRANumber], [Name], [HighNetWorthAuM], [NumberOfHighNetWorthClients], [FINRAStatus])
GO
