CREATE TABLE [STAGE].[FirmAuM]
(
[FirmCRDNumber] [int] NOT NULL,
[HighNetWorthAuM] [decimal] (20, 2) NULL,
[NumberOfHighNetWorthClients] [int] NULL
) ON [PRIMARY]
GO
