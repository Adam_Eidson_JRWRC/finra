CREATE TABLE [dbo].[Individual_BranchLocation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[FirmId] [int] NOT NULL,
[IndividualId] [int] NOT NULL,
[Street1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[postalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedAt] [datetime] NOT NULL CONSTRAINT [DF_Individual_BranchLocation_CreatedAt] DEFAULT (getdate()),
[UpdatedAt] [datetime] NOT NULL CONSTRAINT [DF_Individual_BranchLocation_UpdatedAt] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Individual_BranchLocation] ADD CONSTRAINT [PK__Individu__3214EC07F86083A2] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Individual_BranchLocation] ADD CONSTRAINT [FK_Individual_BranchLocation_Firm] FOREIGN KEY ([FirmId]) REFERENCES [dbo].[Firm] ([FirmId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Individual_BranchLocation] ADD CONSTRAINT [FK_Individual_BranchLocation_Individual] FOREIGN KEY ([IndividualId]) REFERENCES [dbo].[Individual] ([IndividualId]) ON DELETE CASCADE
GO
