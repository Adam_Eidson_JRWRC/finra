SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vIndividualFirmRelationship] as

SELECT f.[Name] as FirmName
      ,f.[SECNumber] as FirmSECNumber
      ,f.[FINRANumber]as FirmFINRANumber
      ,f.[CreatedAt] as FirmCreatedAt
      ,f.[updatedAt] as FirmUpdatedat
      ,f.[FINRAStatus] as FirmFINRAStatus
      ,f.[SECStatus] as FirmSECStatus
      ,f.[HighNetWorthAuM] as FirmHighNetWorthAUM
      ,f.[NumberOfHighNetWorthClients] as FirmNumberOfHighNetWorthClients
	  ,indFm.StartDate as IndividualFirm_StartDate
	  ,indFm.EndDate as IndividualFirm_EndDate
	  ,ind.[CRDNumber]
      ,ind.[SECNumber]
      ,ind.[FirstName]
      ,ind.[LastName]
      ,ind.[MiddleName]
      ,ind.[Suffix]
      ,ind.[SECLink]
      ,ind.[FINRALink]
      ,ind.[CreatedAt]
      ,ind.[updatedAt]
      ,ind.[SECStatus]
      ,ind.[FINRAStatus]
	  ,l.name as LicenseName
	  ,c.Name as CustodianName
	  ,fc.RegulatoryAssetsUndermanagement
	  ,max(fc.FilingDateSubmitted) as mostRecentFilingDateSubmitted
  FROM [FINRA].[dbo].[Firm] f
  join FINRA.dbo.Individual_Firm indFm on indFm.firmId = f.FirmId
  join FINRA.dbo.Individual ind on ind.individualId = indFm.individualId
  join FINRA.dbo.Individual_License il on il.individualId = ind.IndividualId
  join FINRA.dbo.License l on l.licenseId = il.licenseId
  left join FINRA.dbo.Firm_Custodian fc on fc.FirmId = f.FirmId
  left join FINRA.dbo.Custodian c on c.CustodianId = fc.CustodianId
  group by f.[Name] 
      ,f.[SECNumber] 
      ,f.[FINRANumber]
      ,f.[CreatedAt] 
      ,f.[updatedAt] 
      ,f.[FINRAStatus] 
      ,f.[SECStatus] 
      ,f.[HighNetWorthAuM] 
      ,f.[NumberOfHighNetWorthClients] 
	  ,ind.[CRDNumber]
      ,ind.[SECNumber]
      ,ind.[FirstName]
      ,ind.[LastName]
      ,ind.[MiddleName]
      ,ind.[Suffix]
      ,ind.[SECLink]
      ,ind.[FINRALink]
      ,ind.[CreatedAt]
      ,ind.[updatedAt]
      ,ind.[SECStatus]
      ,ind.[FINRAStatus]
	  ,c.Name
	  ,fc.RegulatoryAssetsUndermanagement
	  ,l.name 
	  ,indFm.StartDate 
	  ,indFm.EndDate
GO
