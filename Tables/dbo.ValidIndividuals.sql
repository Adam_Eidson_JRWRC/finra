CREATE TABLE [dbo].[ValidIndividuals]
(
[IndividualCRD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Licenses] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FINRADetailedreportURL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmCRD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighNetWorthAUM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumberofHighNetWorthClients] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
