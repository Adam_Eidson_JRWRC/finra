SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vFirmWebsite] as
SELECT Website, 
Name, 
FINRANumber, 
FINRAStatus
  FROM [FINRA].[dbo].[Firm_Website] fw
  join FINRA.dbo.Firm f on f.FirmId = fw.FirmId
GO
