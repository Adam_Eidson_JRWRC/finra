SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vSalesForceEmailDomainFirmWebsiteRelationship] as

  
  with cte_sf_noCRDNoBD as (
  SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT Name, email, id FROM Contact where CRD__c is null and email is not null and territory__c is null')
  ),
  cte_sf_final as (
  select distinct SUBSTRING(email,charindex('@',email)+1,50) SF_domain,
  id as Contact_Id,
  Name as Contact_Name
  from cte_sf_noCRDNoBD sf)

  select distinct sf.*,
  fw.website,
  f.FINRANumber,
  f.Name as firmName
  from cte_sf_final sf
  join FINRA.dbo.firm_website fw on fw.website like '%www.'+sf.SF_domain+'%'
  join FINRA.dbo.firm f on f.firmId = fw.FirmId
GO
