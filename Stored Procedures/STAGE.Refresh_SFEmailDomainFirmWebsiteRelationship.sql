SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 11/14/2019
-- Description:	Refreshes the STAGE.SFEmailDomainFirmWebsiteRelationship table with valid rows that still need to be processed.
-- =============================================
--exec FINRA.[STAGE].[Refresh_SFEmailDomainFirmWebsiteRelationship]
CREATE PROCEDURE [STAGE].[Refresh_SFEmailDomainFirmWebsiteRelationship]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	truncate table FINRA.STAGE.SFEmailDomainFirmWebsiteRelationship

Insert into FINRA.STAGE.SFEmailDomainFirmWebsiteRelationship
(
SF_domain ,
Contact_Id ,
Contact_Name ,
website ,
FINRANumber ,
firmName 
)
select distinct 
SF_domain ,
Contact_Id ,
Contact_Name ,
website ,
FINRANumber ,
firmName 
from FINRA.dbo.vSalesForceEmailDomainFirmWebsiteRelationship

END

GO
