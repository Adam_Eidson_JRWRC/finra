SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
  CREATE VIEW [dbo].[vSFContactWithCRDFirmNumber] as 
  (
  SELECT distinct fa.state, sf.id, f.FINRANumber
  FROM  OPENQUERY(SALESFORCEDEVART,'SELECT Name, email, CRD__c, id FROM Contact where CRD__c is not null and territory__c is null') sf
  join FINRA.dbo.Individual i on i.CRDNumber = sf.CRD__c
  join FINRA.dbo.Individual_Firm iff on iff.individualId = i.IndividualId
  join FINRA.dbo.Firm_Address fa on fa.FirmId = iff.FirmId
  join FINRA.dbo.Firm f on f.FirmId = fa.FirmId
  where iff.EndDate is null
  and state <> ''

  )
GO
