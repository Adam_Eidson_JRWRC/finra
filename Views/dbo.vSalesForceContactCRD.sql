SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vSalesForceContactCRD] as

  SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT CRD__c FROM Contact where CRD__c is not null')
GO
